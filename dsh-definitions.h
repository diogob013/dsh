/*
 * dsh-definitions.h
 * This file is part of dsh project
 * 
 * (c) 2022 - Diogo B013
 * 
 * dsh is a free command interpreter.
 * You can redistribute it and/or modify.
 * It is licensed under the MIT License.
*/
#ifndef DSH_DEFINITIONS_H
#define DSH_DEFINITIONS_H

#define COMMAND_SIZE 1024
#define ARG_SIZE 256
#define SPLIT_KEYS " \t"

extern char** environ;
/*
    for(size_t i = 0; environ[i] != NULL; ++i){
        printf("%s\n", environ[i]);
    }
*/
#endif
