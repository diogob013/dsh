/*
 * dsh-commands.c
 * This file is part of dsh project
 * 
 * (c) 2022 - Diogo B013
 * 
 * dsh is a free command interpreter.
 * You can redistribute it and/or modify.
 * It is licensed under the MIT License.
*/
#include "dsh-commands.h"
#include "dsh-definitions.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

// Print the command interpreter manual
void show_help(){
    char help_txt[] = {
        "DSH: this is a command interpreter.\n"
        "You need to type an internal command or executable file.\n\n"
        "INTERNAL COMMANDS:\n"
        "ajuda\t Print this help text\n"
        "amb\t Print or set env variable\n"
        "cd\t Change the working directory\n"
        "exit\t Exit from this command interpreter\n"
        "limpar\t Clear the screen\n"
    };
    puts(help_txt);
}

// Change working directory
short change_directory(char *path, char** env_vector, unsigned env_count){
    short error_status = chdir(path);
    if( !error_status ){
        unsigned i;
        char cwd[ARG_SIZE];
        snprintf(cwd, ARG_SIZE, "%s", getcwd(NULL, ARG_SIZE));

        set_env_var("CWD", cwd, env_vector, &env_count);
        for(i = strlen(cwd); (i > 0) && cwd[i] != '/'; i--);
        set_env_var("DIRNAME", cwd+i+1, env_vector, &env_count);
    }
    return error_status;
}


// Clear terminal
void clear_screen(){
    printf("\x1b[H\x1b[2J\x1b[3J");
}

// Run a external executable binary
short run_external_cmd(char **args_vector){
    int pid = fork();

    if(pid > 0){        // Parent process, wait
        waitpid(pid, NULL, 0);
    }
    else if(pid == 0){  // Chil process, run it
        return execvp(args_vector[0], args_vector);
    }else{
        return -2;      // Error on fork
    }
    return 0;
}

// Get an environment variable
char* get_env_var(char* name, char** env_vector, unsigned env_count){
    unsigned pos_eq_sig;
    for(int i = 0; i < env_count; i++){
        pos_eq_sig = strcspn(env_vector[i], "=");
        if( env_vector[i][0] == name[0] &&                  // compare the first letters
            strncmp(env_vector[i], name, pos_eq_sig) == 0   // compare the whole string
        ){
            return env_vector[i]+pos_eq_sig+1;              // return string after name=
        }
    }
    return "";
}

// Set an environment variable.
// Return the new env
char** set_env_var(char* name, char* value, char** env_vector, unsigned* env_count){
    unsigned pos_eq_sig;
    int i;
    short var_already_exists = 0;

    for(i = 0; i < *env_count; i++){
        pos_eq_sig = strcspn(env_vector[i], "=");
        if( env_vector[i][0] == name[0] &&                  // compare the first letters
            strncmp(env_vector[i], name, pos_eq_sig) == 0   // compare the whole string
        ){
            var_already_exists = 1;
            break;
        }
    }
    if(var_already_exists){
        snprintf(                    // overwrite variable
            env_vector[i], ARG_SIZE,
            "%s=%s",
            name, value
        );
    }
    else if( (strlen(name)+strlen("=")+strlen(value)) <= ARG_SIZE){
        char** new_env = realloc(env_vector, (*env_count+2)*sizeof(char*));     // Realloc space for
                                                                                // +1 pointer to string
        if( new_env != NULL){                                                   // if success
            env_vector = new_env;
            env_vector[*env_count] = malloc(ARG_SIZE * sizeof(char));           // alloc space for +1 string
            snprintf(env_vector[*env_count], ARG_SIZE, "%s=%s", name, value);   // save name=value
            env_vector[*env_count+1] = NULL;
            (*env_count)++;
        }
    }
    return env_vector;
}

// Get default environment variables
char** get_default_env(unsigned* env_count){
    char buffer[ARG_SIZE];
    int i = 0;
    *env_count = 5;

    char** env_vector = malloc((*env_count+1) * sizeof(char*));
    for(i = 0; i < *env_count; i++)
        env_vector[i] = malloc(ARG_SIZE * sizeof(char));
    env_vector[*env_count] = NULL;

    snprintf(env_vector[0], ARG_SIZE, "PATH=%s", "/bin:/usr/bin:/sbin:/usr/sbin");
    snprintf(env_vector[1], ARG_SIZE, "CWD=%s", getcwd(NULL, ARG_SIZE));
    gethostname(buffer, ARG_SIZE);
    snprintf(env_vector[2], ARG_SIZE, "HOST=%s", buffer);
    for(i = strlen(env_vector[1]); (i > 0) && env_vector[1][i] != '/'; i--);
    snprintf(env_vector[3], ARG_SIZE, "DIRNAME=%s", env_vector[1]+i+1);
    snprintf(env_vector[4], ARG_SIZE, "SHELL=dsh");

    return env_vector;
}

// Create an argV from the string
char** string_to_args_vector(char *txt, int* args_count){
    int numFragments = 1;

    char *pChar = strpbrk(txt, SPLIT_KEYS);
    while( pChar != NULL){                              // Count the number of fragments on txt
        numFragments++;
        pChar = strpbrk(pChar+1, SPLIT_KEYS);
    }
    if(args_count != NULL){
        *args_count = numFragments;
    }

    char** splitTxt;                                    // alloc size equivalent to "char splitTxt[numFragments+1][ARG_SIZE];""
    splitTxt = malloc((numFragments+1) * sizeof(char*));// "numFragments+1" strings with "ARG_SIZE" characters each one
    for (int i = 0; i < numFragments; i++){
        splitTxt[i] = malloc(ARG_SIZE * sizeof(char));
        for(int j=0; j < ARG_SIZE; j++){
            splitTxt[i][j] = '\0';
        }
    }
    splitTxt[numFragments] = NULL;                      // IMPORTANT!!! argv require a "NULL" mark at the end
                                                        //     without it, unexpected errors may occur 
    unsigned keyFoundAt;
    unsigned prevKeySuccessor = 0;
    for(int i = 0; i < numFragments-1; i++){            // Fill splitTxt with text fragments
        keyFoundAt = strcspn(txt+prevKeySuccessor, SPLIT_KEYS);
        strncpy(splitTxt[i], txt+prevKeySuccessor, keyFoundAt);
        prevKeySuccessor += keyFoundAt+1;
    }
    strncpy(splitTxt[numFragments-1], txt+prevKeySuccessor, ARG_SIZE);

    return splitTxt;
}

// Replace $VAR with its value
short replace_env_vars(char *txt, char **env_vars, unsigned env_count){
    unsigned replaced_vars = 0;
    unsigned txt_length = strlen(txt);

    // remove characters that has no graphical representation at the end
    for(int i = txt_length; i >= 0 && !isgraph(txt[i]); i--){
        txt[i] = '\0';
    }

    char var_name[ARG_SIZE];
    for(int dollar_pos=0; dollar_pos < txt_length; dollar_pos++){       // Walking through all chars
        if(txt[dollar_pos] == 36){                                      // if found '$', a variable ($...)
            int var_name_len;
            for(var_name_len = 0; isalnum(txt[dollar_pos+1+var_name_len]); var_name_len++){
                var_name[var_name_len] = txt[dollar_pos+1+var_name_len];// copy var name until something that is not alphanumeric
            }

            int txt_part2_length = strlen(txt+dollar_pos+1+var_name_len);
            char txt_part2_buff[txt_part2_length];
            strcpy(txt_part2_buff, txt+dollar_pos+1+var_name_len);      // save the text after var_name

            snprintf(
                txt+dollar_pos,                                          // overwrite text after '$'
                ARG_SIZE-dollar_pos,
                "%s%s",
                get_env_var(var_name, env_vars, env_count),     // with var content
                txt_part2_buff                                  // and remaining text
            );
            replaced_vars++;
        }
    }
    return replaced_vars;
}

// Frees dynamically allocated vector
void free_dynamic_vector(char **args_vector, int args_count){
    if(args_vector != NULL){
        for(int i = args_count; i >= 0; i--){
            if(args_vector[i] != NULL){
                free(args_vector[i]);
            }
        }
    }
}
