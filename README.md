# dsh - interpretador de comandos simples

Este é um exercício proposto na disciplina de **Sistemas Operacionais** do [**Instituto de Informática**](https://inf.ufg.br/) da [**Universidade Federal de Goiás**](https://www.ufg.br/). O objetivo principal deste trabalho é aplicar de forma prática os conhecimentos relativos a **processos** da disciplina de Sistemas Operacionais.

## Como compilar e executar?

Os requisitos são:
* [Git](https://git-scm.com/)
* [GNU Make](https://www.gnu.org/software/make/)
* Um Compilador para **C**
    * [GCC](https://gcc.gnu.org/) ou
    * [Clang](https://clang.llvm.org/) ou
    * [MinGW](https://www.mingw-w64.org/)

### Instalação das ferramentas
* No **Windows** utilizando [Chocolatey](https://chocolatey.org/install#individual).

Execute o comando no PowerShell
```powershell
> choco install git mingw make
```

* No **Ubuntu, Debian, Mint, PopOS**...</br>

Execute o comando no Terminal
```bash
$ sudo apt update && sudo apt install git build-essential
```

* No **MacOS** </br>

Instale [Command Line Tools](https://developer.apple.com/download/all/) ou [Xcode](https://developer.apple.com/download/all/)

### Compilando e executando
Basta clonar o repositório e utilizar o comando make para compilar
```bash
$ git clone https://gitlab.com/diogob003/dsh.git
$ cd dsh/
$ make
$ ./dsh
```

## License
This project is licensed under the terms of the [MIT](https://choosealicense.com/licenses/mit/) license.
