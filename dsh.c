/*
 * dsh.c
 * This file is part of dsh project
 * 
 * (c) 2022 - Diogo B013
 * 
 * dsh is a free command interpreter.
 * You can redistribute it and/or modify.
 * It is licensed under the MIT License.
*/
#include "dsh-commands.h"
#include "dsh-definitions.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>

char **env_vars;
unsigned env_count;

short dsh_run(FILE *cmd_src_file, FILE* cmd_history){
    char cmd_line[COMMAND_SIZE];
    char prev_cmd_line[COMMAND_SIZE];
    short status_code = 0;
    char **args_vector;
    int args_count = 0;

    if(cmd_src_file == stdin){
        printf(
            "\x1b[1;32m%s >: \x1b[0m",              // visible only when reading from stdIn (keyBoard)
            get_env_var("DIRNAME", env_vars, env_count)
        );
    }
    fscanf(cmd_src_file, " %[^\n]", cmd_line);

    // command history
    if( strcmp(prev_cmd_line, cmd_line) != 0){          // previous copy != actual command
        fprintf(cmd_history, "%s\n", cmd_line);         //   write on file
        strncpy(prev_cmd_line, cmd_line, COMMAND_SIZE); //   do a copy
    }

    args_vector = string_to_args_vector(cmd_line, &args_count);
    for(int i=0; i < args_count; i++){
        replace_env_vars(args_vector[i], env_vars, env_count);
    }

    // Compare if is a dsh internal command or external command
    if( strncmp("ajuda", args_vector[0], strlen(args_vector[0])) == 0){
        if(args_count == 1){
            show_help();
        }else{
            printf(
                "%s: ilegal use of %s command! Type ajuda for help.\n",
                args_vector[0], args_vector[0]
            );
        }
    }
    else if( strncmp("amb", args_vector[0], strlen(args_vector[0])) == 0){
        char **argv_u_vars = string_to_args_vector(cmd_line, NULL); // vector with no env variables replaced
        if(args_count == 1){                                        // if no args, show all
            for(int i=0; i < env_count; i++){
                puts(env_vars[i]);
            }
        }else if(args_count > 1){
            if(argv_u_vars[args_count-1][0] == '$'){                // if args and vars ($...)
                char* var_name = argv_u_vars[args_count-1]+1;       // "$name"+1 = "name"
                printf(
                    "amb: \n"
                    "%s=%s\n",
                    argv_u_vars[args_count-1],
                    get_env_var(var_name, env_vars, env_count)
                );
            }else if(strchr(argv_u_vars[args_count-1], '=') != NULL){
                unsigned pos_eq_sig = strcspn(argv_u_vars[args_count-1], "=");
                argv_u_vars[args_count-1][pos_eq_sig] = '\0';   // "Split" string replacing '=' by '\0'
                env_vars =  set_env_var( argv_u_vars[args_count-1], argv_u_vars[args_count-1]+1+pos_eq_sig,
                                            env_vars, &env_count);
            }
        }else{
            printf(
                "%s: ilegal use of %s command! Type ajuda for help.\n",
                args_vector[0], args_vector[0]
            );
        }
        free_dynamic_vector(argv_u_vars, args_count);
    }
    else if( strncmp("cd", args_vector[0], strlen(args_vector[0])) == 0){
        status_code = change_directory(cmd_line+3, env_vars, env_count);

        if( status_code ){
            fprintf(stderr, "cd: %s: %s\n", strerror(errno), cmd_line+3);
        }
    }
    else if( strncmp("limpar", args_vector[0], strlen(args_vector[0])) == 0){
        if(args_count == 1){
            clear_screen();
        }else{
            printf(
                "%s: ilegal use of %s command! Type ajuda for help.\n",
                args_vector[0], args_vector[0]
            );
        }
    }
    else if( strncmp("exit", args_vector[0], strlen(args_vector[0])) == 0 || feof(cmd_src_file) ){
        if(args_count == 1){
            if(cmd_src_file == stdin){  // reading from stdIn (keyBoard) ?
                puts("Saindo...");      //   print message
            }
            fclose(cmd_history);
            fclose(cmd_src_file);
            return 0;                   // Quit from command interpreter
        }else{
            printf(
                "%s: ilegal use of %s command! Type ajuda for help.\n",
                args_vector[0], args_vector[0]
            );
        }
    }
    else{   // External command
        status_code = run_external_cmd(args_vector);

        if( status_code == -2 ){
            fprintf(stderr, "dsh: %s\n", strerror(errno));
        }else if( status_code){
            fprintf(stderr, "dsh: command not found: %s\n", args_vector[0]);
            return status_code;     // kill forked child process after exec*() found erro
        }
    }

    dsh_run(cmd_src_file, cmd_history);             // loop that breaks when "exit" command is typed
    return 0;
}

int main(int argc, char *argv[]){
    FILE *cmd_src_file = stdin;
    FILE *cmd_history = fopen(".dsh_history", "a+");
    env_vars = get_default_env(&env_count);

    if( argc > 1 ){                                 // There is args?
        cmd_src_file = fopen(argv[argc-1], "r");    // open last argument as a file

        if(cmd_src_file == NULL){                   // error when opening file?
            fprintf(stderr, "dsh: %s: %s", argv[argc-1], strerror(errno));
            return -1;                              
        }
    }

    return dsh_run(cmd_src_file, cmd_history);      // run shell mode
}
