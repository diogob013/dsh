/*
 * dsh-commands.h
 * This file is part of dsh project
 * 
 * (c) 2022 - Diogo B013
 * 
 * dsh is a free command interpreter.
 * You can redistribute it and/or modify.
 * It is licensed under the MIT License.
*/
#ifndef DSH_COMMANDS_H
#define DSH_COMMANDS_H

// Print the command interpreter manual
void show_help();

// Change Directory
short change_directory(char *path, char** env_vector, unsigned env_count);

// Clear terminal
void clear_screen();

// Run a external executable binary
short run_external_cmd(char **args_vector);

// Get an environment variable
char* get_env_var(char* name, char** env_vector, unsigned env_count);

// Set an environment variable.
// Return the new env
char** set_env_var(char* name, char* value, char** env_vector, unsigned* env_count);

// Get default environment variables
char** get_default_env(unsigned* env_count);

// Create an argV from the string
char** string_to_args_vector(char *txt, int* args_count);

// Replace $VAR with its value
short replace_env_vars(char *txt, char **env_vars, unsigned env_count);

// Frees dynamically allocated vector
void free_dynamic_vector(char **args_vector, int args_count);

#endif
