# Variáveis
CC = gcc
CFLAGS = -pedantic-errors -Wall -std=c99

# Regra : dependências
all: dsh-commands.o dsh.o
	$(CC) $(CFLAGS) dsh.o dsh-commands.o -o dsh

dsh.o: dsh.c
	$(CC) $(CFLAGS) -c dsh.c -o dsh.o

dsh-commands.o: dsh-commands.c
	$(CC) $(CFLAGS) -c dsh-commands.c -o dsh-commands.o

clean:
	rm -rf *.o dsh

run:
	./dsh

# See target-specific variables https://www.gnu.org/software/make/manual/make.html#Target_002dspecific
debug: CFLAGS += -g
debug: clean all
